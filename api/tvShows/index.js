import express from "express";
import tvShowModel from './tvShowModel';
import { getTvReviews } from "../tmdb-api";

const router = express.Router();

router.get('/', (req, res, next) => {
    tvShowModel.find().then(tvShows => res.status(200).send(tvShows)).catch(next);
});

router.get('/:id', (req, res, next) => {
    const id = parseInt(req.params.id);
    tvShowModel.findByTvShowDBId(id).then(tvShow => res.status(200).send(tvShow)).catch(next);
});

router.get("/:id/reviews", (req, res, next) => {
    const id = parseInt(req.params.id);
    getTvReviews(id)
        .then((TvReviews) => res.status(200).send(TvReviews))
        .catch((error) => next(error));
});

router.get('/airing', (req, res, next) => {
    tvShowModel.find().then(tvShows => res.status(200).send(tvShows)).catch(next);
});

router.get('/topRated', (req, res, next) => {
    tvShowModel.find().then(tvShows => res.status(200).send(tvShows)).catch(next);
});

export default router;