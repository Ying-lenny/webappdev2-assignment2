import chai from "chai";
import request from "supertest";

const expect = chai.expect;

let api;
let token;

const samplePerson = {
    id: 287,
    name: "Brad Pitt",
};

describe("People endpoint", () => {
    beforeEach(async () => {
        try {
            api = require("../../../../index");
        } catch (err) {
            console.error(`failed to Load user Data: ${err}`);
        }
    });

    afterEach(() => {
        api.close(); // Release PORT 8080
        delete require.cache[require.resolve("../../../../index")];
        return true;
    });

    describe("GET /people ", () => {
        it("should return 20 people and a status 200", () => {
            request(api)
                .get("/api/people")
                .set('Authorization', 'Bearer ' + token)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((err, res) => {
                    expect(res.body).to.be.a("array");
                    expect(res.body.length).to.equal(20);  
                }); 
        }); 
    });
});

    describe("GET /people/:id", () => {
        describe("when the id is valid", () => {
            it("should return the matching person", () => {
                request(api)
                    .get(`/api/people/${samplePerson.id}`)
                    .set('Authorization', 'Bearer ' + token)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .then((res) => {
                        expect(res.body).to.have.property("name", samplePerson.name);
                    });
            });
        });

        describe("when the id is invalid", () => {
            it("should return the NOT found message", () => {
                request(api)
                    .get("/api/people/xxx")
                    .set('Authorization', 'Bearer ' + token)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect({
                        success: false,
                        status_code: 34,
                        status_message: "The resource you requested could not be found.",
                    });
            });
        });
    });
