# Assignment 2 - Agile Software Practice.

Name: Owen Lenihan

## Target Web API.

...... Document the Web API that is the target for this assignment's CI/CD pipeline. Include the API's endpoints and any other features relevant to the creation of a suitable pipeline, e.g.

+ Get /api/movies - returns an array of movie objects.
+ Get /api/movies/:id - returns detailed information on a specific movie.
+ Get /api/movies/:id/reviews - returns all reviews made for a specific movie called by ID
+ Get /api/movies/upcoming - returns a list of movies yet to be released

+ Get /api/users - returns a list of all the registered users on the system
+ Post /api/users - creates a new user provided it has the proper authentication
+ Put /api/users - Updates the store of users on mongo
+ Get /api/users/userName/favourites - returns list of an Id'd user's favourite movies
+ Post /api/users/userName/favourites - Adds new movie to list of user's favourite movies

+ Get /api/people - returns a list of popular actors in the system
+ Get /api/people/:id - returns specific details about a 

+ Get /api/tvShows - returns a list of multiple tv shows
+ Get /api/tvShows/:id - Returns specific information about an Id'd TV show
+ Get /api/tvShows/:id/reviews - Returns reviews on an Id'd TV show
+ Get /api/airing - Returns a list of the currently airing TV shows
+ Get /api/topRated - Returns a list of the highest rated TV shows in the database

## Error/Exception Testing.

.... From the list of endpoints above, specify those that have error/exceptional test cases in your test code, the relevant test file and the nature of the test case(s), e.g.

+ Post /api/movies - test when the new movie has no title, invalid release date, empty genre list. Test adding a movie without prior authentication. See tests/functional/api/movies/index.js 

## Continuous Delivery/Deployment.

..... Specify the URLs for the staging and production deployments of your web API, e.g.

+ https://dashboard.heroku.com/apps/movies-api-stagin - Staging deployment
+ https://movies-api-production.herokuapp.com/ - Production

.... Show a screenshots from the overview page for the two Heroku apps e,g,

+ Staging app overview 

![][stagingapp]

![][stagingapp2]

[If an alternative platform to Heroku was used then show the relevant page from that platform's UI.]

## Feature Flags (If relevant)

... Specify the feature(s) in your web API that is/are controlled by a feature flag(s). Mention the source code files that contain the Optimizerly code that implement the flags. Show screenshots (with appropriate captions) from your Optimizely account that prove you successfully configured the flags.


[stagingapp]: ./public/HerokuStagingpng.png
[stagingapp2]: ./public/HerokuStaging2png.png